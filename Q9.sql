--Question 9
SELECT
    t.category_name,
    SUM(t1.item_price) AS price_total
FROM
    item_category t 
LEFT JOIN 
    item t1
ON
    t.category_id = t1.category_id
GROUP BY
    t.category_name
ORDER BY
	price_total DESC;
